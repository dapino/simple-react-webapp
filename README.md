# Simple. React web app
This app is developed in React 16.10, uses [React Hooks](https://reactjs.org/docs/hooks-intro.html).

Uses [Formik](https://github.com/jaredpalmer/formik) to manage forms and [yup](https://github.com/jquense/yup) to validate forms with object schema. 

[React Router](https://github.com/ReactTraining/react-router) form router mangement.

[Styled Components](https://github.com/styled-components/styled-components) to stylize the components 😛.

## Install
Preferably use YARN as a package manager:
Install YARN
```curl -o- -L https://yarnpkg.com/install.sh | bash```

Install devendencies:

```yarn install```

Run app in dev mode:

```yarn start```

