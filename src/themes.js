// import { pxToRem } from "./utils";

const colors = {
    white: "#fff",
    black: "#000",
    gray: "#ccc",
    primaryColor: "#00b7a2",
    primaryColorDarken: "#0f3a3a",
    secondaryColor: "#e08126",

};

const themes = {
    dark: {
        colors: colors,
        primaryColor: colors.primaryColor,
        primaryColorDarken: colors.primaryColorDarken,
        secondaryColor: colors.secondaryColor
    },
    light: {
        colors: colors,
        primaryColor: colors.primaryColor,
        primaryColorDarken: colors.primaryColorDarken,
        secondaryColor: colors.secondaryColor,
        headerTextColor: colors.black,
        pageBackground: colors.gray,
        cardShadow: `0px 4px 5px -4px ${colors.black}`,
        dropdownMenuShadow: `3px 3px 10px -2px ${colors.darkgray}`
    }

};

export default themes;
