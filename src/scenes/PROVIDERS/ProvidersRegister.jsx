import React from "react";
import SignUpForm from "../../components/Forms/SignUpForm";
import SelectSkus from "../../components/Forms/SelectSkus";
import UploadDocuments from "../../components/Forms/UploadDocuments";
import { H1 } from "../../components/text-ui-components/H1";
import {apiProviders} from "../../services/providers";
import toaster from "toasted-notes";

const ProvidersRegister = () => {


    return (
        <>
            <H1>Registro de proveedor</H1>
            <SignUpForm/>
            <SelectSkus/>
            <UploadDocuments />
        </>
    )
};

export default ProvidersRegister;
