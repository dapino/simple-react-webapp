import React, {Component, useCallback, useState, useEffect} from "react";
import styled from "styled-components";
import CategoriesForm from "../../components/Forms/CategoriesForm";
import {H1} from "../../components/text-ui-components/H1";
import {skusServices} from "../../services/skus";
import toaster from "toasted-notes";
import PaginationTable from "../../components/table/PaginationTable";

const Hero = styled.div`
    background: blue;
`;

const userToken = localStorage.getItem('userToken');
const columns = [
    {Header: "Grupo", accessor: "group_id"},
    {Header: "ID", accessor: "id"},
    {Header: "Nombre", accessor: "name"},
    {
        Header: "Acciones",
        Cell: ({row}) => {
            const {original} = row;
            const buttons = [
                {
                    id: original._id,
                    iconName: "edit",
                    title: "Actualizar",
                    handleAction: async () => {
                        history.push(`/contact/${original._id}`);
                    }
                },
                {
                    id: original._id,
                    iconName: "delete",
                    title: "Eliminar",
                    handleAction: async () => AlertThenDelete(original._id)
                }
            ];
            return <p>editar</p>;
        }
    }
]

class Categories extends Component {
    constructor(props) {
        super(props);
        this.state = { categories: [] };
    }

    componentDidMount() {
        this.listCategories().then( r => this.setState({ categories: r}))
    }

    listCategories = async () => {
        const response = await skusServices.getCategories();
        if (!response.error) {
        console.log(response);
            return response;
        } else {
            toaster.notify(`Hubo un error: ${response.error}`, {
                duration: 8000,
                position: "bottom-right"
            });
        }
    };


    render() {

        return (
            <>
                <H1>Categories</H1>
                <CategoriesForm/>
                <PaginationTable columns={columns} data={this.state.categories}/>
            </>
        )
    }
}


export default Categories;
