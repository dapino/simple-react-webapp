import React from "react";
import SignInForm from "../../components/Forms/SignInForm";
import {userServices} from "../../services/users";
import toaster from "toasted-notes";
import { withRouter } from "react-router-dom";

const SignIn = props => {
    const signIn = async data => {
        const response = await userServices.signIn(data);
        if (!response.error) {
            localStorage.setItem('userToken', response.token);
            toaster.notify(`Bienvenido de nuevo ${response.user.name}`, {
                duration: 4000,
                position: "bottom-right"
            });
            props.history.push("/");
        } else {
            toaster.notify(`Hubo un error: ${response.error}`, {
                duration: 8000,
                position: "bottom-right"
            });
        }
    };
    return (
        <>
            <SignInForm signInFunc={signIn}/>
        </>
    )
};

export default withRouter(SignIn);
