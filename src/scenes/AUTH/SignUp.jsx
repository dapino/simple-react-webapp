import React from "react";
import SignUpForm from "../../components/Forms/SignUpForm";
import {userServices} from "../../services/users";
import toaster from "toasted-notes";
import { withRouter } from "react-router-dom";

const SignUp = props => {
    const signUp = async data => {
        const response = await userServices.signUp(data);
        if (!response.error) {
            console.log(response);
            toaster.notify(`Tu registro ha sido exitoso`, {
                duration: 4000,
                position: "bottom-right"
            });
            props.history.push("/login");
        } else {
            toaster.notify(`Hubo un error en el registro: ${response.error}, intenta de nuevo por favor`, {
                duration: 8000,
                position: "bottom-right"
            });
        }
    };

    return (
        <>
            <SignUpForm signUpFunc={signUp}/>
        </>
    )
};

export default withRouter(SignUp);
