import React, {useState, useMemo, useEffect, createContext} from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {createGlobalStyle, ThemeProvider} from "styled-components";
import themes from "./themes";
import Home from "./scenes/Home";
import BasicLayout from "./components/layouts/BasicLayout";
import ProvidersRegister from "./scenes/PROVIDERS/ProvidersRegister";
import SignIn from "./scenes/AUTH/SignIn";
import SignUp from "./scenes/AUTH/SignUp";
import Categories from "./scenes/SKUS/Categories";
import {Footer} from "./components/Layouts/Footer";
import Header from "./components/Layouts/Header";

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Bree+Serif|Dosis&display=swap');
  * { 
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-family: 'Dosis',Helvetica,Arial,Lucida,sans-serif;
   }
  .App {
    align-items: center;
    font-size: ${props => props.fontSize};
    font-family: 'Roboto', sans-serif;
    width: 99vw;
    height: 99vh;
    margin: 0;
  }
`;

const themeContext = {
    theme: themes.light,
    toogleTheme: () => {
    }
};

export const UserContext = createContext(null)
export const ThemeContext = createContext(themeContext);

const App = () => (
    <ThemeProvider theme={themeContext.theme}>
        <BrowserRouter>
            <BasicLayout>
                <Header/>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route path='/provider' component={ProvidersRegister} />
                    <Route path='/login' component={SignIn} />
                    <Route path='/signup' component={SignUp} />
                    <Route path='/categories' component={Categories} />
                </Switch>
            </BasicLayout>
            <Footer/>
        </BrowserRouter>
        <GlobalStyle />
    </ThemeProvider>
);

export default App;
