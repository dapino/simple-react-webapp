import React from 'react';
import styled from "styled-components";

export const Button = styled.button`
    background-color: ${props => props.theme.secondaryColor};
    border: none;
    border-radius: 3px;
    color: white;
    cursor: pointer;
    padding: .5em 1.5em;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: auto;
    box-shadow: 0 2px 5px -2px #666;
    opacity: 0.8;
    &:hover {
      opacity: 1;
      box-shadow: 0 2px 3px -2px #444;
    }
`;
