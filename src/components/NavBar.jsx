import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

export const Menu = styled.div`
   display: flex;
   
`;

const Navbar = () => (
    <Menu>
        <Link to="/signup">SignUp</Link>
        <Link to="/provider">Register as a provider</Link>
        <Link to="/login">Login</Link>
        <Link to="/categories">Categories</Link>
    </Menu>
);

export default Navbar;
