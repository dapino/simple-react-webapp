import React from 'react';
import styled from "styled-components";

export const Form = styled.form`
    display: flex;
    padding: 1em;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    background-color: rgba(255,255,255,0.5);
    max-width: 95%;
    width: 500px;
    margin: 2em auto;
    box-shadow: 0 2px 4px -2px #999;
`;
