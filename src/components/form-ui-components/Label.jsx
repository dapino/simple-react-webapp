import React from 'react';
import styled from "styled-components";

export const Label = styled.label`
    color: #666;
    font-size: 16px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    margin: .6em auto;
    position: relative;
    padding-bottom: 1.2em;
    width: 90%;
`;
