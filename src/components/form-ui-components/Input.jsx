import React from 'react';
import styled from "styled-components";

export const Input = styled.input`
    border: 1px solid #ddd;
    border-radius: 5px;
    color: #666;
    padding: .6rem 1em;
    text-decoration: none;
    display: inline-block;
    font-size: 1em;
    width: 100%;
    box-shadow: 0 2px 1px -2px #999;
    margin-top: .2em;
`;
