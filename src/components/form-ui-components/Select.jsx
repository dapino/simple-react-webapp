import React from 'react';
import styled from "styled-components";

export const Select = styled.select`
    border: 1px solid #ccc;
    outline: none;
    color: #666;
    padding: .5rem 2em;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    box-shadow: 0 2px 1px -2px #000;
    width: 100%;
    margin-top: .2em;
`;
