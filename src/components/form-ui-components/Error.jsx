import React from 'react';
import styled from "styled-components";

export const Error = styled.span`
    color: red;
    font-size: 14px;
    position: absolute;
    bottom: 0;
`;
