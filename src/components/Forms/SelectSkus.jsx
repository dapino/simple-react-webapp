import React from "react";
import { Formik, Field, ErrorMessage} from "formik";
import * as Yup from "yup";


import { Input } from "../form-ui-components/Input";
import { Select } from "../form-ui-components/Select";
import { Form } from "../form-ui-components/Form";
import { Label } from "../form-ui-components/Label";
import { Error } from "../form-ui-components/Error";
import { Button } from "../Button";

import { apiProviders } from "../../services/providers";

const SelectSkus = () => {
    /**
    //TODO
     * Create setState hook to getListCategories to fill out the categories select
     * Create effect hook to listen categoriesSelect onChange and getListSubcategories
     * Create effect hook to listen subcategoriesSelect onChange and getListSkus
     * Create array with selected skus
     * Render list of selected skus
     * Connect to createProvider service and send the skus array to perform the create
     * Redirect to add documents
     */

    /*const selectedSkus = {
        "details": [
            {
                "sku": {
                    "id": 41
                }
            },
            {
                "sku": {
                    "id": 43
                }
            },
            {
                "sku": {
                    "id": 44
                }
            }
        ]
    }*/

    const getCategories = () => {

    }

    const getSubcategories = () => {

    }

    const getSkus = () => {

    }

    /*const createProvider = async data => {
        const response = await apiProviders.createRequest(selectedSkus);
        if (!response.error) {
            console.log(response)
            //history.push("/");
        }
    };*/

    return (
        <Formik
            initialValues={{ categories: "", subcategories: "", skus: ""}}
            validationSchema={Yup.object({
                categories: Yup.string()
                    .required('Required'),
                subcategories: Yup.string()
                    .required('Required'),
                skus: Yup.string()
                    .required('Required')
            })}
            onSubmit={(values, { setSubmitting })  => {
                console.log(values)
                // createProvider(values);
                setSubmitting(false)
            }}
        >
                <Form>
                    <Label htmlFor="categories">Categorías
                        <Select name="categories">
                            <option value="red">Hogar</option>
                            <option value="green">Mecánica</option>
                        </Select>
                        <ErrorMessage name="categories"/>
                    </Label>
                    <Label htmlFor="subcategories">Subcategorías
                        <Select name="subcategories">
                            <option value="red">Hogar</option>
                            <option value="green">Mecánica</option>
                        </Select>
                        <ErrorMessage name="subcategories"/>
                    </Label>
                    <Label htmlFor="skus">Skus
                        <Select name="skus">
                            <option value="red">Hogar</option>
                            <option value="green">Mecánica</option>
                        </Select>
                        <ErrorMessage name="skus"/>
                    </Label>
                    <Button type="submit">Guardar Skus</Button>
                </Form>

        </Formik>
    )
};

export default SelectSkus;
