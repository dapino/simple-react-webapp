import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import toaster from "toasted-notes";

import { Input } from "../form-ui-components/Input";
import { Select } from "../form-ui-components/Select";
import { Form } from "../form-ui-components/Form";
import { Label } from "../form-ui-components/Label";
import { Error } from "../form-ui-components/Error";
import { Button } from "../Button";

import {userServices} from "../../services/users";


const CategoriesForm = () => {
    const signIn = async data => {
        const response = await userServices.signIn(data);
        if (!response.error) {
            console.log(response);
            toaster.notify(`Bienvenido de nuevo ${response.user.name}`, {
                duration: 4000,
                position: "bottom-right"
            });
        } else {
            toaster.notify(`Hubo un error: ${response.error}`, {
                duration: 8000,
                position: "bottom-right"
            });
        }

    };

    return (
        <Formik
            initialValues={{ email: 'hola@dapino.co', password: '123456'}}
            validationSchema={Yup.object({
                name: Yup.string()
                    .required('Required'),
                css_class: Yup.string()
                    .required('Required'),
                group_id: Yup.string()
                    .required('Required')
            })}
            onSubmit={(values, { setSubmitting })  => {
                signIn(values);
                setSubmitting(false)
            }}
        >

            {formik => (
                <Form onSubmit={formik.handleSubmit}>
                    <Label htmlFor="name">Name
                        <Input id="name" {...formik.getFieldProps('name')} />
                        {formik.touched.name && formik.errors.name ? <Error>{formik.errors.name}</Error> : null}
                    </Label>

                    <Label htmlFor="group_id">Grupo
                        <Select id="group_id" {...formik.getFieldProps('group_id')}>
                            <option value="1">Hogar</option>
                            <option value="2">Mecánica</option>
                        </Select>
                        {formik.touched.group_id && formik.errors.group_id ? <Error>{formik.errors.group_id}</Error> : null}
                    </Label>

                    <Label htmlFor="css_class">Class
                        <Input id="css_class" {...formik.getFieldProps('css_class')} />
                        {formik.touched.css_class && formik.errors.css_class ? <Error>{formik.errors.css_class}</Error> : null}
                    </Label>

                    <Button type="submit">Crear categoría</Button>
                </Form>
            )}
        </Formik>
    )
};

export default CategoriesForm;
