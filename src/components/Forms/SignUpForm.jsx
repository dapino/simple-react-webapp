import React from "react";
import { Formik, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

import { Input } from "../form-ui-components/Input";
import { Form } from "../form-ui-components/Form";
import { Label } from "../form-ui-components/Label";
import { Error } from "../form-ui-components/Error";
import { Button } from "../Button";

const SignUpForm = props => (
    <Formik
        initialValues={{ name: 'tst09', email: 'tst09@gmail.com', password: '123456', passwordConfirm: ''}}
        validationSchema={Yup.object({
            name: Yup.string()
                .min(4, 'Must be 4 characters or more')
                .required('Required'),
            email: Yup.string()
                .email('Invalid email addresss')
                .required('Required'),
            password: Yup.string()
                .min(6, 'Must be 6 characters or more')
                .required('Required'),
            passwordConfirm: Yup.string()
                .oneOf([Yup.ref('password'), null], 'Passwords must match')
                .required('Required'),
        })}
        onSubmit={(values, { setSubmitting })  => {
            props.signUpFunc(values);
            setSubmitting(false)
        }}
    >

    {formik => (
        <Form onSubmit={formik.handleSubmit}>
            <Label htmlFor="name">Name
                <Input id="name" {...formik.getFieldProps('name')} />
                {formik.touched.name && formik.errors.name ? <Error>{formik.errors.name}</Error> : null}
            </Label>
            <Label htmlFor="email">Email
                <Input id="email" {...formik.getFieldProps('email')} />
                {formik.touched.email && formik.errors.email ? <Error>{formik.errors.email}</Error> : null}
            </Label>
            <Label htmlFor="password">Password
                <Input id="password" {...formik.getFieldProps('password')} />
                {formik.touched.password && formik.errors.password ? <Error>{formik.errors.password}</Error> : null}
            </Label>

            <Label htmlFor="passwordConfirm">Password
                <Input id="passwordConfirm" {...formik.getFieldProps('passwordConfirm')} />
                {formik.touched.passwordConfirm && formik.errors.passwordConfirm ? <Error>{formik.errors.passwordConfirm}</Error> : null}
            </Label>
            <Button type="submit">Registrarme</Button>
        </Form>
    )}
    </Formik>
);

export default SignUpForm;
