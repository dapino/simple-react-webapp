import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";

import { Input } from "../form-ui-components/Input";
import { Form } from "../form-ui-components/Form";
import { Label } from "../form-ui-components/Label";
import { Error } from "../form-ui-components/Error";
import { Button } from "../Button";

const SignInForm = props => (
    <Formik
        initialValues={{ email: 'hola@dapino.co', password: '123456'}}
        validationSchema={Yup.object({
            email: Yup.string()
                .email('Invalid email addresss')
                .required('Required'),
            password: Yup.string()
                .min(6, 'Must be 6 characters or more')
                .required('Required'),
        })}
        onSubmit={(values, { setSubmitting })  => {
            props.signInFunc(values);
            setSubmitting(false)
        }}
    >
        {formik => (
            <Form onSubmit={formik.handleSubmit}>
                <Label htmlFor="email">Email
                    <Input id="email" {...formik.getFieldProps('email')} />
                    {formik.touched.email && formik.errors.email ? <Error>{formik.errors.email}</Error> : null}
                </Label>
                <Label htmlFor="password">Password
                    <Input id="password" type="password" {...formik.getFieldProps('password')} />
                    {formik.touched.password && formik.errors.password ? <Error>{formik.errors.password}</Error> : null}
                </Label>
                <Button type="submit">Iniciar sesión</Button>
            </Form>
        )}
    </Formik>
);

export default SignInForm;
