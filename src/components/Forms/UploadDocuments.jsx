import React, {Component} from "react";
import { Formik } from "formik";
import * as Yup from "yup";

import { apiProviders } from "../../services/providers";

import { Input } from "../form-ui-components/Input";
import { Form } from "../form-ui-components/Form";
import { Label } from "../form-ui-components/Label";
import { Error } from "../form-ui-components/Error";
import { Button } from "../Button";
import toaster from "toasted-notes";


class UploadDocuments extends Component {
    constructor(props) {
        super(props);
        this.state = { file: "" };
    }

    onSubmit = async () => {
        console.log(this.state.file)
        const response = await apiProviders.uploadDocs(this.state.file);
        if (!response.error) {
            console.log(response)
        } else {
            toaster.notify(`Hubo un error: ${response.error}`, {
                duration: 8000,
                position: "bottom-right"
            });
        }
    };


    render() {
        return (
            <Formik
                initialValues={{ documentName: "diploma", document: ""}}
                validationSchema={Yup.object({
                    documentName: Yup.string()
                        .min(4, 'the name must be at least 4 characters')
                        .required('Required'),
                    document: Yup.mixed().required()
                })}
                onSubmit={(values, { setSubmitting })  => {
                    console.log(values)
                    setSubmitting(false)
                }}
            >
                {formik => (
                    <>
                        <Label htmlFor="documentName">documentName
                            <Input id="documentName" {...formik.getFieldProps('documentName')} />
                            {formik.touched.documentName && formik.errors.documentName ? <Error>{formik.errors.documentName}</Error> : null}
                        </Label>
                        <Label htmlFor="document">Documento
                            <Input name="document" type="file" className="uploadFile" onChange={(event) => {
                                this.setState({"file": event.currentTarget.files[0]})
                            }} />
                            {formik.touched.document && formik.errors.document ? <Error>{formik.errors.document}</Error> : null}
                        </Label>

                        <Button onClick={e => this.onSubmit()}>Subir documentos</Button>
                    </>
                )}
            </Formik>
        )
    }
};

export default UploadDocuments;
