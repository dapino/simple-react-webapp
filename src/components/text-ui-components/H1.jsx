import React from 'react';
import styled from "styled-components";

export const H1 = styled.h1`
    margin: 1rem auto;
    font-size: 2em;
    color: #333;
    text-align: center;
    font-family: 'Bree Serif',Georgia,"Times New Roman",serif;
`;
