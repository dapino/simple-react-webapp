import React from "react";
import styled from "styled-components";

import LogoSimple from "../assets/logo-simple.svg";

const StyledLogo = styled.img`
    opacity: 0.8;
    &:hover {
      opacity: 1;
      box-shadow: 0 2px 3px -2px #444;
    }
`;

const Logo = () => (
    <StyledLogo src={LogoSimple}/>
)

export default Logo;
