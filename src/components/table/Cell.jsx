import styled from "styled-components";
import { pxToRem } from "../../utils";

export const StyledCell = styled.td`
  padding: ${pxToRem(12)} ${pxToRem(8)};
  border: ${pxToRem(1)} solid;
  color: ${props => props.theme.colors.darkGrey};
  border-color: ${props => props.theme.complementaryColor};
`;
