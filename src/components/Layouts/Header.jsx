import React from 'react';
import styled from "styled-components";
import {pxToRem} from "../../utils";
import Navbar from "../NavBar";
import Logo from "../Logo";

const StyledHeader = styled.div`
  display: flex;
  align-items: center;
  background-color: ${props => props.theme.primaryColor};
  box-shadow: 0 -2px 7px -1px #333;
  height: ${pxToRem(60)};
  justify-content: space-between;
  padding: ${pxToRem(16)};
  position: fixed;
  right: 0;
  top: 0;
  left: 0;
  z-index: 99;
`;

const Header = () => (
    <StyledHeader>
        <Logo/>
        <Navbar/>
    </StyledHeader>
)

export default Header;

