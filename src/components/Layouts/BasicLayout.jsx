import React from 'react';
import styled from "styled-components";

const BasicLayout = styled.div`
   background: #edf8ff;
   padding: 70px 1em 2em;
`;

export default BasicLayout;
