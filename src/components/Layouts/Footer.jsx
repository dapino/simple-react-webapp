import React from 'react';
import styled from "styled-components";
import {pxToRem} from "../../utils";

export const Footer = styled.div`
  background-color: ${props => props.theme.primaryColorDarken};
  height: ${pxToRem(30)};
  padding: ${pxToRem(16)};
  position: relative;
  z-index: 99;
`;


