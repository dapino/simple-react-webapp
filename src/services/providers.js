/* Defined Constants */
import {URL_API} from "../config/const";
/* Defined Endpoints */
import endpoints from "../config/endpoints";

const fetchParams = (method, data = "") => {
    const body = data ? {body: JSON.stringify(data)} : {};
    return {
        method,
        headers: apiHeaders,
        credentials: "same-origin",
        ...body
    };
};

let apiHeaders = {
    "Content-Type": "application/json",
    Accept: "application/json",
};

export const apiProviders = {
    /*24*/
    uploadDocsDeprecated: async (documentName, document, requestID) => {
        const body = new FormData();
        const userToken = localStorage.getItem('userToken');
        apiHeaders = { ...apiHeaders, 'Authorization': userToken, 'Content-Type': 'multipart/form-data'  };

        body.append('name', documentName.value);
        body.append('file', document.files[0]);
        body.append('id', "24");
        body.append('enctype', 'multipart/form-data');

        try {
            const response = await fetch(`${URL_API}${endpoints.providers.documents}`, fetchParams('POST', body));
            const data = await response.json();
            return data;
        } catch (error) {
            throw error.toString();
        }
    },
    uploadDocs: async (file) => {
        const userToken = localStorage.getItem('userToken');

        const dataf = new FormData();
        dataf.append("file", file);
        dataf.append('name', 'documento de prueba');
        dataf.append('id', "24");
        const newApiHeaders = new Headers();
        newApiHeaders.append("Accept", "application/json");
        newApiHeaders.append("Authorization", userToken);
        const params = {
            method: "POST",
            headers: newApiHeaders,
            credentials: "same-origin",
            body: dataf
        };
        try {
            const response = await fetch(`${URL_API}${endpoints.providers.documents}`, params);
            const data = await response.json();
            return data;
        } catch (error) {
            console.error(error);
            return error;
        }
    },





};
