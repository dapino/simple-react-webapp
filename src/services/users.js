import { URL_API } from "../config/const";
import endpoints from "../config/endpoints";

const fetchParams = (method, data = "") => {
    const body = data ? { body: JSON.stringify(data) } : {};
    return {
        method,
        headers: apiHeaders,
        credentials: "same-origin",
        ...body
    };
};

let apiHeaders = {
    "Content-Type": "application/json",
    Accept: "application/json",
    Authorization: ""
};

export const userServices = {
    uploadFile: async (file, fileName, token) => {
        let dataf = new FormData();
        dataf.append(fileName, file);
        apiHeaders.append('Authorization', token);
        let path = pathByFilename[fileName];
        let url = `${URL_API}${path}`;
        let params = {
            'method': 'POST',
            'headers': apiHeaders,
            'credentials': 'same-origin',
            'body': dataf
        };
        try {
            const response = await fetch(url, params);
            const data = await response.json();
            return data;
        } catch (error) {
            console.error(error);
        }

    },
    signIn: async (user) => {
        apiHeaders = { ...apiHeaders };
        try {
            const response = await fetch(`${URL_API}${endpoints.auth.signIn}`, fetchParams('POST', { ...user }));
            const data = await response.json();
            return data;
        } catch (error) {
            throw error.toString();
        }
    },
    signUp: async (user) => {
        apiHeaders = { ...apiHeaders };
        try {
            const response = await fetch(`${URL_API}${endpoints.auth.signUp}`, fetchParams('POST', { ...user }));
            const data = await response.json();
            return data;
        } catch (error) {
            throw error.toString();
        }
    },
};
