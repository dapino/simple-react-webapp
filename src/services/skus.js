import { URL_API } from "../config/const";
/* Defined Endpoints */
import endpoints from "../config/endpoints";

const fetchParams = (method, data = "") => {
    const body = data ? { body: JSON.stringify(data) } : {};
    return {
        method,
        headers: apiHeaders,
        credentials: "same-origin",
        ...body
    };
};

let apiHeaders = {
    "Content-Type": "application/json",
    Accept: "application/json",
    Authorization: ""
};

export const skusServices = {
    getCategories: async () => {
        apiHeaders = { ...apiHeaders };
        try {
            const response = await fetch(`${URL_API}${endpoints.categories.list}`, fetchParams('GET'));
            const data = await response.json();
            return data;
        } catch (error) {
            throw error.toString();
        }
    }

};
