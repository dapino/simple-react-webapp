export const URL_API = "https://dev-simple.herokuapp.com/api";

export const BASE_FONT_SIZE = 16;

export const menuItems = [
    {
        title: "Proveedor",
        iconName: "table_chart",
        path: "/provider",
        submenus: [
            {
                title: "item1",
                iconName: "view_column",
                path: "/item"
            }
        ]
    }
];
