export default {
    auth: {
        signIn: '/login',
        signUp: '/registry',
        password: '/users/forgot-password',
    },
    user: {
        update: '/profile',
        updatePassword: '/profile/update-password'
    },
    providers: {
        request: '/request-providers',
        list: '/v1/providers',
        documents: '/request-providers/documents'

    },
    groups: {
        create: '/groups',
        list: '/groups-list',
    },
    categories: {
        request: '/categories',
        list: '/categories-list',
    },
    subcategories: {
        request: '/subcategories',
        list: '/subcategories-list',
    },
    skus: {
        request: '/skus',
        list: '/skus-list',
    }

};
